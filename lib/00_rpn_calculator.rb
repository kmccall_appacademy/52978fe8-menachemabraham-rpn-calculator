require 'byebug'

class RPNCalculator
  OPERATORS = %w[+ - * /]

  def initialize
  @stack = []
  end

  def push(token)
    @stack << token
  end

  def plus
    calculate &:+
  end

  def minus
    calculate &:-
  end

  def times
    calculate &:*
  end

  def divide
    calculate &:/
  end

  def value
    @stack.last
  end

  def tokens(token_str)
    token_str.split.map do |token|
      operator?(token) ? token.to_sym : token.to_f
    end
  end

  def evaluate(token_str)
    tokens(token_str).each do |token|
      operator?(token) ? calculate(&token) : push(token)
    end

    value
  end

  private
  def calculate(&operation)
    raise "calculator is empty" if @stack.size < 2

    operand2 = @stack.pop.to_f
    operand1 = @stack.pop.to_f
    @stack << operation.call(operand1, operand2)
  end

  def operator?(token)
    OPERATORS.include?(token.to_s)
  end
end
